
/*'***************************************************************
'Script Name    	: Scr_Arbella_Login
'Purpose    		: Login Connecticut and Submit the Connecticut form
'Developed by  		: Nagesh Kamma
'Developed Date 	: 13/03/2019
'TestDataSheet 		: Login
'TestCase Path Excel : C:\Users\E001506\Desktop\DemoFinal\demo back project\New folder\Arbella_Connecticut\Data\Arbella\TestData.xls
*/
package com.Arbella.tests;

import org.testng.annotations.Test;

import com.Arbella.businessFunctions.BusinessFunctions;
import com.Arbella.utilities.Data_Provider;

public class Arbella_Login extends BusinessFunctions{
	@Test
	public void Arbella_login() throws Throwable{
	try{
		//Read the excel data
		tstData = Data_Provider.getTestData("Login", "Arbella_Login");
		String username = tstData.get("UserName");	
		String password = tstData.get("Password");
		String AgencyAddress = tstData.get("AgencyAddress");
		String ProducerCode = tstData.get("ProducerCode");
		String LicensedProducer = tstData.get("LicensedProducer");
		String NodeID = tstData.get("NodeID");
		String FirstName = tstData.get("FirstName");
		String LastName = tstData.get("LastName");
		String PhoneNumber = tstData.get("PhoneNumber");
		String EmailAddress = tstData.get("EmailAddress");
		String HOForm = tstData.get("HOForm");
		String ApplicantInformationFirstName = tstData.get("ApplicantInformationFirstName");
		String ApplicantInformationLastName = tstData.get("ApplicantInformationLastName");
		String NameofPriorCarrier = tstData.get("NameofPriorCarrier");
		String CurrentOccupation = tstData.get("CurrentOccupation");
		String CurrentEmployer = tstData.get("CurrentEmployer");
		String CoApplicant = tstData.get("CoApplicant");
		String ApplicantStreetName = tstData.get("ApplicantStreetName");
		String priorcarrierName = tstData.get("priorcarrierName");
		String PriorBodilyInjuryLimits = tstData.get("PriorBodilyInjuryLimits");
		String ZIPCode = tstData.get("ZIPCode");
		String CityandState = tstData.get("CityandState");
		String DaytimePhonNumber = tstData.get("DaytimePhonNumber");
		String LicenseNumber = tstData.get("LicenseNumber");
		String ApplicantNumber = tstData.get("ApplicantNumber");
		String DLicenseNumber = tstData.get("DLicenseNumber");
		String LicenseState = tstData.get("DriverLicenseState");
		String Gender = tstData.get("DriverGender");
		String MaritalStatus = tstData.get("DriverMaritalStatus");
		String Occupation = tstData.get("DriverOccupation");
		String HighestEducationLevel = tstData.get("DriverHighestEducationLevel");
		String DriverValidationMessage = tstData.get("DriverValidationMessage");
		String ValidDLicenseNumber = tstData.get("ValidDLicenseNumber");
		String DriverDob = tstData.get("DriverDob");
		String VehicleVinNumber = tstData.get("VehicleVinNumber");
		String VehicleType = tstData.get("VehicleType");
		String VehicleYear = tstData.get("VehicleYear");
		String VehicleMake = tstData.get("VehicleMake");
		String VehicleModel = tstData.get("VehicleModel");
		String VehicleCollisionSymbol = tstData.get("VehicleCollisionSymbol");
		String VehicleComprehensiveSymbol = tstData.get("VehicleComprehensiveSymbol");
		String BodilyInjury = tstData.get("BodilyInjury");
		String PropertyDamage = tstData.get("PropertyDamage");
		String UMSplit = tstData.get("UMSplit");
		String UMCSL = tstData.get("UMCSL");
		String PrincipalOperator = tstData.get("PrincipalOperator");
		String Usage = tstData.get("Usage");
		String AnnualMileage = tstData.get("AnnualMileage");
		String BillingMethod = tstData.get("BillingMethod");
		String PaymentPlan = tstData.get("PaymentPlan");
		String DownPaymentType = tstData.get("DownPaymentType");
		String DownPaymentAmount = tstData.get("DownPaymentAmount");
		
	
		
		//This Method is Used to Launch the Application
		ArbellalaunchURL();
		//This Method is used to Login into the application
		
		Arbella_Login(username,password);
		
		//This Method is used to Arbella CT Form		
		SubmitArbellaCTForm(AgencyAddress,ProducerCode,LicensedProducer,NodeID,FirstName,LastName,PhoneNumber,EmailAddress,HOForm,ApplicantInformationFirstName,ApplicantInformationLastName,NameofPriorCarrier,CurrentOccupation,CurrentEmployer,CoApplicant,ApplicantStreetName,priorcarrierName,PriorBodilyInjuryLimits,ZIPCode,CityandState,DaytimePhonNumber,ApplicantNumber,DLicenseNumber,LicenseState,Gender,MaritalStatus, Occupation,HighestEducationLevel,DriverValidationMessage,ValidDLicenseNumber,DriverDob,VehicleVinNumber,VehicleType,VehicleYear,VehicleMake,VehicleModel,VehicleCollisionSymbol,VehicleComprehensiveSymbol,BodilyInjury,PropertyDamage,UMSplit,UMCSL,PrincipalOperator,Usage,AnnualMileage,BillingMethod,PaymentPlan,DownPaymentType,DownPaymentAmount);
	
	}
	catch(Exception e){
		failureReport("Submit Arbella CT Form", "Failed to Submit Arbella CT Form");
	}
 }
}