
/*'***************************************************************
'Script Name    	: Scr_Arbella_Login
'Purpose    		: Login Connecticut
'Developed by  		: Nagesh Kamma
'Developed Date 	: 13/03/2019
'TestDataSheet 		: Login
'TestCase Path Excel : 
*/
package com.Arbella.tests;

import org.testng.annotations.Test;

import com.Arbella.businessFunctions.BusinessFunctions;
import com.Arbella.utilities.Data_Provider;

public class Arbella_Logininfailed extends BusinessFunctions{
	@Test
	public void Arbella_Logininfailed() throws Throwable{
	try{
		tstData = Data_Provider.getTestData("Login", "Arbella_Login");
		String username = tstData.get("UserName");	
		String password = tstData.get("Password");
		ArbellalaunchURL();
		ArbellaLoginInvalid(username,password);
	}
	catch(Exception e){
		failureReport("Error in ToDos validation", "Arbella Login");
	}
 }

	
}