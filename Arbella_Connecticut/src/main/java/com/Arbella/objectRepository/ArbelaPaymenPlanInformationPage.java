package com.Arbella.objectRepository;

import org.openqa.selenium.By;

public class ArbelaPaymenPlanInformationPage {
	
	public static By BillingMethod =By.xpath("//select[contains(@name,'billingMethod') and contains(@id,'billingMethod')]");
	public static By PaidinFullDiscount =By.xpath("//input[contains(@name,'pifDiscount') and contains(@id,'pifDiscount_1')]");
	
	public static By PaymentPlan =By.xpath("//select[contains(@name,'paymentPlan') and contains(@id,'paymentPlan')]");
	public static By DownPaymentType =By.xpath("//select[contains(@name,'downPaymentType') and contains(@id,'downPaymentType')]");
	public static By DownPaymentAmount =By.xpath("//input[contains(@name,'downpaymentAmount') and contains(@id,'downpaymentAmount')]");
	
		
	
}
