package com.Arbella.objectRepository;

import org.openqa.selenium.By;

public class ArbelaPolicySummaryObjects {
	
	public static By PolicySummaryAgreecheckbox = By.xpath("//input[contains(@name,'agree') and contains(@id,'agree')]");
	public static By AryarbellaSummaryButton = By.xpath("//input[contains(@class,'arbellaSummaryButton btn btn-primary') and contains(@id,'submitForReview')]");
	public static By SubmissionConfirmation = By.xpath("//div[@id=\"transaction\"]/div/div[1]/div[1]/div/div[2]/h1");
	public static By saveAndExitButton = By.xpath("//input[@name='saveAndExitButton']");
}
