package com.Arbella.objectRepository;

import org.openqa.selenium.By;

public class ArbelaVehiclesCoveragesInformationPage {
	
	public static By VehicleVinNumber =By.xpath("//input[contains(@name,'VehicleVIN') and contains(@id,'VehicleVIN')]");
	public static By VehicleType =By.xpath("//select[contains(@name,'VehicleType') and contains(@id,'VehicleType')]");
	public static By VehicleYear=By.xpath("//input[contains(@name,'VehicleYear') and contains(@id,'VehicleYear')]");
	public static By VehicleMake =By.xpath("//input[contains(@name,'VehicleMake') and contains(@id,'VehicleMake')]");
	public static By VehicleModel =By.xpath("//input[contains(@name,'VehicleModel') and contains(@id,'VehicleModel')]");	
	public static By VehicleCollisionSymbol =By.xpath("//input[contains(@name,'VehicleCollisionSymbol') and contains(@id,'VehicleCollisionSymbol')]");
	public static By VehicleBodilyInjury =By.xpath("//select[contains(@name,'VehicleBodilyInjury') and contains(@id,'VehicleBodilyInjury')]");
	public static By VehiclePropertyDamage =By.xpath("//select[contains(@name,'VehiclePropertyDamage') and contains(@id,'VehiclePropertyDamage')]");
	
	
	public static By VehicleComprehensiveSymbol =By.xpath("//input[contains(@name,'VehicleComprehensiveOTCSymbol') and contains(@id,'VehicleComprehensiveOTCSymbol')]");	
	//public static By VehicleBIPDSymbol =By.xpath("//select[contains(@name,'DriverMaritalStatus') and contains(@id,'DriverMaritalStatus')]");
	//public static By VehicleBRBMPSymbol =By.xpath("//select[contains(@name,'DriverOccupation') and contains(@id,'DriverOccupation')]");
	//public static By VehicleLeasedYESNO =By.xpath("//select[contains(@name,'highesteducationlevel') and contains(@id,'highesteducationlevel')]");
	
	
	//public static By VehicleAnnualMileage =By.xpath("//select[contains(@name,'DriverGender') and contains(@id,'DriverGender')]");	
	//public static By VehicleBIPDSymbol =By.xpath("//select[contains(@name,'DriverMaritalStatus') and contains(@id,'DriverMaritalStatus')]");
	//public static By VehicleBRBMPSymbol =By.xpath("//select[contains(@name,'DriverOccupation') and contains(@id,'DriverOccupation')]");
	//public static By VehicleLeasedYESNO =By.xpath("//select[contains(@name,'highesteducationlevel') and contains(@id,'highesteducationlevel')]");
	
	public static By UMSplit =By.xpath("//select[contains(@id,'VehicleUMSplit') ]");
	public static By UMCSL =By.xpath("//select[contains(@id,'VehicleUMCSL') ]");

	public static By PrincipalOperator =By.xpath("//select[contains(@name,'VehiclePrincipalRatedDriver') and contains(@id,'VehiclePrincipalRatedDriver')]");
	public static By Usage =By.xpath("//select[contains(@name,'VehicleUsage') and contains(@id,'VehicleUsage')]");
	public static By AnnualMileage =By.xpath("//input[contains(@name,'VehicleAnnualMileage') and contains(@id,'VehicleAnnualMileage')]");
	
	
	
}
