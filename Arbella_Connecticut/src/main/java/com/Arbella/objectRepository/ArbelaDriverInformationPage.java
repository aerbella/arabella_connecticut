package com.Arbella.objectRepository;

import org.openqa.selenium.By;

public class ArbelaDriverInformationPage {
	
	public static By DriverDetailFirstName =By.xpath("//input[@name='DriverFirstName']");
	public static By DriverDetailLastName =By.xpath("//input[@name='DriverLastName']");
	public static By DriverDob=By.xpath("//input[@name='DriverDateOfBirthUnsecured']");
	public static By DriverLicenseNumber =By.xpath("//input[@name='DriverLicenseNumberUnsecured']");
	public static By DriverLicenseState =By.xpath("//select[contains(@name,'DriverLicenseState') and contains(@id,'DriverLicenseState')]");	
	public static By DateFirstLicensed =By.xpath("//input[@name='DriverDateLicensedUnsecured']");
	
	public static By DriverGender =By.xpath("//select[contains(@name,'DriverGender') and contains(@id,'DriverGender')]");	
	public static By DriverMaritalStatus =By.xpath("//select[contains(@name,'DriverMaritalStatus') and contains(@id,'DriverMaritalStatus')]");
	public static By DriverOccupation =By.xpath("//select[contains(@name,'DriverOccupation') and contains(@id,'DriverOccupation')]");
	public static By DriverHighestEducationLevel =By.xpath("//select[contains(@name,'highesteducationlevel') and contains(@id,'highesteducationlevel')]");
	
	public static By DriverClickAddButton =By.xpath("//input[contains(@name,'addButton') and contains(@id,'addButton')]");
	public static By Driverliceancevalidationmessage =By.xpath("//*[@id='alert-danger']/p");
	public static By DriverLicenseNumbervalidation =By.xpath("//span[@class='problemField']//div");
	public static By DriverLicenseNumberLabelClick =By.xpath("//*[@id='DriverLicenseNumberUnsecured_labelText']");
	
	
}
