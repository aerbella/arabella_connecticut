package com.Arbella.objectRepository;

import org.openqa.selenium.By;

public class ArbelaLoginObjects {
	
	public static By UserName = By.xpath("//input[@name='username']");
	public static By Password = By.xpath("//input[@name='password']");
	public static By LoginButton = By.xpath("//input[@type='submit']");
	public static By UserNameInvalid = By.xpath("//input[@name='usernametest']");
}
