package com.Arbella.objectRepository;

import org.openqa.selenium.By;

public class ArbelaAgencyInformationPage {
	
	public static By SelectAgencyAddress =By.xpath("//select[contains(@name,'Producer.GeneralPartyInfo.Addr.Addr1') and contains(@id,'Producer_GeneralPartyInfo_Addr_Addr1')]");
	public static By SelectProducerCode =By.xpath("//span[contains(@id,'select2-chosen')]");
	public static By SelectProducerCode1 =By.xpath("//div[contains(@id,'select2-result-label')]/span");
	//public static By SelectProducerCode =By.xpath("//div[contains(@class,'select2-container')and contains(@id,'s2id_producerCode')]");
	public static By SelectLicensedProducer =By.xpath("//select[contains(@class,'form-control') and contains(@id,'producerName')]");
	public static By SelectNodeID =By.xpath("//select[contains(@name,'nodeId') and contains(@id,'nodeId')]");
	public static By ClickProducerCode =By.xpath("//a[@class=\"select2-choice select2-default\"]");
	public static By FirstName =By.xpath("//input[@name='ApplicantFirstName']");
	public static By LastName =By.xpath("//input[@name='ApplicantLastName']");
	public static By IsThereaCoApplicant =By.xpath("//select[contains(@name,'IsCoApplicant') and contains(@id,'IsCoApplicant')]");
	public static By ClickApplicantInformation =By.xpath("//fieldset[@id='AppInfoSection']//legend");
	public static By ClickResidentialAddressInformation =By.xpath("//fieldset[@id='StreetSection']//legend");
	public static By appsubmittedclientrecentely =By.xpath("//select[contains(@name,'recentappl') and contains(@id,'recentappl')]");
	public static By CityandState =By.xpath("//select[contains(@name,'ApplicantCityState') and contains(@id,'ApplicantCityState')]");
	public static By CityandStateContactaddress =By.xpath("//select[contains(@name,'StreetCityState') and contains(@id,'StreetCityState')]");
	public static By PhoneNum =By.xpath("//input[@name='AgencyPhoneNum']");
	public static By EmailAddress =By.xpath("//input[@name='AgencyEmailAdd']");
	public static By PolicyEffectiveDate =By.xpath("//input[@name='EffectiveDate']");
	public static By SelectHOForm =By.xpath("//select[contains(@name,'HOForm') and contains(@id,'HOForm')]");
	public static By NameofPriorCarrier =By.xpath("//select[contains(@name,'PriorCarrierName') and contains(@id,'PriorCarrierName')]");
	public static By ApplicantInformationFirstName =By.xpath("//input[@name='ApplicantFirstName']");
	public static By ApplicantInformationLastName =By.xpath("//input[@name='ApplicantLastName']");
	public static By ApplicantInformationDateofBirth =By.xpath("//input[@name='ApplicantDOB']");
	public static By YearswithCurrentOccupation =By.xpath("//input[@name='ApplicantYearsInCurrentOccupation']");
	public static By YearswithCurrentEmployer =By.xpath("//input[@name='ApplicantYearsWithCurrentEmployer']");
	//public static By IsThereaCoApplicant =By.xpath("//select[contains(@name,'IsCoApplicant') and contains(@id,'IsCoApplicant')]");
	public static By ContinueButton =By.xpath("//input[@name='continueButton']");
	public static By ContinueQuoteSummary  =By.xpath("//input[@name='continue']");
	public static By ClickZIPCodewindow =By.xpath("//span[@id='ApplicantZipCode_labelText']");
	public static By selectCalender =By.xpath("//*[@id='policyTerm']/div[1]/div/span/div/span/i");
	public static By SelectToday =By.xpath("//div[@class='datepicker-days']//td[@class='today day']");
	public static By ApplicantStreetName =By.xpath("//input[@name='ApplicantStName']");
	public static By ApplicantNumber =By.xpath("//input[@name='ApplicantStNum']");
	public static By ZIPCode =By.xpath("//input[@name='ApplicantZipCode']");
	//public static By ResidentialAddressContactRadiobutton  =By.xpath("//input[@name='RiskCheckbox']");
	public static By ResidentialAddressContactRadiobutton  =By.xpath("//*[@id='RiskCheckbox']");
	//*[@id="RiskCheckbox"]
	public static By DaytimePhonNumber =By.xpath("//input[contains(@name,'ApplicantDayPhone') and contains(@id,'ApplicantDayPhone')]");
	
	
	
	
	
}
