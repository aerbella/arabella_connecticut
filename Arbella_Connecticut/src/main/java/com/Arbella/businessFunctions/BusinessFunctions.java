package com.Arbella.businessFunctions;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;

import org.apache.poi.ss.formula.functions.Now;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;

import com.Arbella.accelerators.ActionEngine;
import com.Arbella.accelerators.CredentialManager;
import com.Arbella.objectRepository.ArbelaAgencyInformationPage;
import com.Arbella.objectRepository.ArbelaAgentHomePage;
import com.Arbella.objectRepository.ArbelaDriverInformationPage;
import com.Arbella.objectRepository.ArbelaLoginObjects;
import com.Arbella.objectRepository.ArbelaPaymenPlanInformationPage;
import com.Arbella.objectRepository.ArbelaPolicySummaryObjects;
import com.Arbella.objectRepository.ArbelaPriorCarrierInformationPage;
import com.Arbella.objectRepository.ArbelaUnderwritingquestionPage;
import com.Arbella.objectRepository.ArbelaVehiclesCoveragesInformationPage;
import com.mysql.jdbc.util.VersionFSHierarchyMaker;
import com.thoughtworks.selenium.webdriven.commands.Click;

public class BusinessFunctions extends ActionEngine {

//	public String holdName;
	/**
	 * To Get the Current Date in Number Format
	 * 
	 * @return
	 * @throws Throwable
	 */
	public String getDateInFormat(String actualDate, String format) throws Throwable {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String dateInString = actualDate;
		String convertedDate = null;
		try {

			Date date = formatter.parse(dateInString);
			convertedDate = formatter.format(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return convertedDate;
	}

	/**
	 * @Description: Launching Arbella URL
	 * @throws Throwable
	 */
	public void ArbellalaunchURL() throws Throwable {
		try {
			
			propertyManager = CredentialManager.getProperty("Properties");
			credentialManager = CredentialManager.getProperty(propertyManager.get("Environment"));
			//System.out.println("URL: " + credentialManager.get("Arbella_URL"));
			launchUrl(credentialManager.get("Arbella_URL"));
			SuccessReportWithScreenshot("Launch URL", "Sucessfully  launched  Arbella URL");
		} catch (Exception e) {
			failureReport("Launch URL", "Failed to launch the Arbella URL");
		}
	}

	public static void  titleValidation(WebDriver driver, String exp_title)
	{
		String act_title=driver.getTitle();
		Assert.assertEquals(exp_title, act_title);
	}

	/**
	 * @Description : Enter userName and password and login to Arbella Application
	 * @param userName
	 * @param password
	 * @throws Throwable
	 */
	public void Arbella_Login(String username, String password) throws Throwable {

		try {
			driver.findElement(ArbelaLoginObjects.UserName).clear();
			type(ArbelaLoginObjects.UserName, username, "Usrername");
			//driver.findElement(ArbelaLoginObjects.UserName).sendKeys(username);
			SuccessReportWithScreenshot("Enter UserName", "Sucessfully Entered UserName");
			type(ArbelaLoginObjects.Password, password, "Password");
			/*driver.findElement(ArbelaLoginObjects.Password).clear();
			driver.findElement(ArbelaLoginObjects.Password).sendKeys(password);*/
			SuccessReportWithScreenshot("Enter Password", "Sucessfully Entered Password");
			//WebDriverWait wait = new WebDriverWait(driver, 30);
		
			click(ArbelaLoginObjects.LoginButton, "Click on login button");
			SuccessReportWithScreenshot("LoginButton", "login button cliked Sucessfully");
					} catch (Exception e) {
			e.printStackTrace();
			failureReport("Login validation", "Failed to Login into Arbella Application");
		}
	}
	
	public void ArbellaLoginInvalid(String username, String password) throws Throwable {

		try {
			driver.findElement(ArbelaLoginObjects.UserName).clear();
			type(ArbelaLoginObjects.UserNameInvalid, username, "Usrername");
			//driver.findElement(ArbelaLoginObjects.UserName).sendKeys(username);
			SuccessReportWithScreenshot("Enter UserName", "Sucessfully Entered UserName");
			type(ArbelaLoginObjects.Password, password, "Password");
			/*driver.findElement(ArbelaLoginObjects.Password).clear();
			driver.findElement(ArbelaLoginObjects.Password).sendKeys(password);*/
			SuccessReportWithScreenshot("Enter Password", "Sucessfully Entered Password");
			//WebDriverWait wait = new WebDriverWait(driver, 30);
		
			click(ArbelaLoginObjects.LoginButton, "Click on login button");
			SuccessReportWithScreenshot("LoginButton", "login button cliked Sucessfully");
					} catch (Exception e) {
			e.printStackTrace();
			failureReport("Login validation", "Failed to Login into Arbella Application");
		}
	}
	

	public void SubmitArbellaCTForm(String AgencyAddress,String ProducerCode,String LicensedProducer,String NodeID,String FirstName,String LastName,String PhoneNumber,String EmailAddress, String HOForm,String ApplicantInformationFirstName,String ApplicantInformationLastName,String NameofPriorCarrier,String CurrentOccupation,String CurrentEmployer,String CoApplicant,String ApplicantStreetName,String priorcarrierName,String PriorBodilyInjuryLimits,String ZIPCode,String CityandState,String DaytimePhonNumber,String ApplicantNumber,String DLicenseNumber,String LicenseState,String Gender,String MaritalStatus,String Occupation,String HighestEducationLevel,String DriverValidationMessage,String ValidDLicenseNumber,String DriverDob,String VehicleVinNumber,String VehicleType,String VehicleYear,String VehicleMake,String VehicleModel,String VehicleCollisionSymbol,String VehicleComprehensiveSymbol,String BodilyInjury,String PropertyDamage,String UMSplit,String UMCSL,String PrincipalOperator,String Usage,String AnnualMileage,String BillingMethod,String PaymentPlan,String DownPaymentType,String DownPaymentAmount) throws Throwable {

		try {
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Navigating to New Business page
			click(ArbelaAgentHomePage.NewBusinessButton,"Click on NewBusiness Button");
			SuccessReportWithScreenshot("Connecticut NewBusiness Page", "Sucessfully Navigate to Connecticut NewBusiness");
			// Navigating to Connecticut Homeowners
			click(ArbelaAgentHomePage.ConnecticutPersonalAuto,"Click on Connecticut PersonalAuto link");
			SuccessReportWithScreenshot("Connecticut PersonalAuto Page", "Sucessfully Navigate to Connecticut PersonalAuto page");
			//Select  SelectProducerCode
		    click(ArbelaAgencyInformationPage.SelectProducerCode,"Click on ProducerCode text box");
			click(ArbelaAgencyInformationPage.SelectProducerCode1,"Select value on ProducerCode text box");
			Thread.sleep(3000);
			//Scroll Down the page
			js.executeScript("window.scrollBy(0,1000)");
			//Select the current date
			selectEffectiveDate(driver);
	        Thread.sleep(5000);
	        //Click on the ApplicantInformation page
			click(ArbelaAgencyInformationPage.ClickApplicantInformation,"ClickApplicantInformation");
			//Fill Applicant FirstName Information
			type(ArbelaAgencyInformationPage.ApplicantInformationFirstName,FirstName, "FirstName");
			//Fill Applicant LastName Information
			type(ArbelaAgencyInformationPage.ApplicantInformationLastName,LastName, "LastName");
			//Select the today date
			selectEffectiveDateApplicationinformationDOB(driver);
			//Select  Is there a Co-Applicant yes or no dropdown
		    selectByVisibleText(ArbelaAgencyInformationPage.IsThereaCoApplicant,CoApplicant, "Co Applicant information");
		    SuccessReportWithScreenshot("Connecticut Applicant Information Page", "Sucessfully Navigate to Applicant Information page");
		    //Click on continue button
			click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
			//Select CoApplicant submitted client recently
			Thread.sleep(10000);
			//Select app submitted client recentely check box
			selectByVisibleText(ArbelaAgencyInformationPage.appsubmittedclientrecentely,CoApplicant, "Select Has an application recently been submitted for this client");
			//Fill ApplicantStreetName Information
			type(ArbelaAgencyInformationPage.ApplicantStreetName,ApplicantStreetName, "ApplicantStreetName");
			//Fill ApplicantNumber Information
			type(ArbelaAgencyInformationPage.ApplicantNumber,ApplicantNumber, "ApplicantNumber");
			//Fill ZIPCode Information
			type(ArbelaAgencyInformationPage.ZIPCode,ZIPCode, "ZIPCode");
			//Click on ZIPCode window 	
			click(ArbelaAgencyInformationPage.ClickZIPCodewindow,"Click on ZIPCode window");
			Thread.sleep(10000);
			//Select City and State drop down
			selectByVisibleText(ArbelaAgencyInformationPage.CityandState,CityandState, "City and State");
			//Select Day time PhonNumber	
			type(ArbelaAgencyInformationPage.DaytimePhonNumber,DaytimePhonNumber, "DaytimePhonNumber");
			//Scroll Down the page
			js.executeScript("window.scrollBy(0,800)");
			//Select Residential Address "Click here to use the Contact" radio button
			click(ArbelaAgencyInformationPage.ClickResidentialAddressInformation,"ClickResidentialAddressInformation");
			//Click Residential Address Contact Radio button
			click(ArbelaAgencyInformationPage.ResidentialAddressContactRadiobutton,"Click Residential Address Contact Radio button");
			Thread.sleep(10000);
			//Select City and State Contact address
			selectByVisibleText(ArbelaAgencyInformationPage.CityandStateContactaddress,CityandState, "City and State");
			//Click on continue button
			click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
			Thread.sleep(10000);
			//Select prior carrier Name
			selectByVisibleText(ArbelaPriorCarrierInformationPage.priorcarrierName,priorcarrierName, "prior carrier Name");
			//Select Prior Bodily Injury Limits
			selectByVisibleText(ArbelaPriorCarrierInformationPage.PriorBodilyInjuryLimits,PriorBodilyInjuryLimits, "Arbela PriorCarrier Information Page");
			SuccessReportWithScreenshot("Connecticut PriorCarrier Information Page", "Sucessfully Navigate to PriorCarrier Information page");
			 
			//Click on continue button
			click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
			//Fill DriverDetailFirstName Information
			type(ArbelaDriverInformationPage.DriverDetailFirstName,ApplicantInformationFirstName, "DriverDetailFirstName");
			//Fill DriverDetailLastName Information
			type(ArbelaDriverInformationPage.DriverDetailLastName,ApplicantInformationLastName, "DriverDetailLastName");
			//Scroll Down the page
			js.executeScript("window.scrollBy(0,200)");
			//Select the Current date
			selectEffectiveDateDriverDetailsinformationDOB(driver);
			//Fill DLicenseNumber Information
			type(ArbelaDriverInformationPage.DriverLicenseNumber,DLicenseNumber, "DriverLicenseNumber"); 
			//Click  Driver License Number Label
			click(ArbelaDriverInformationPage.DriverLicenseNumberLabelClick,"DriverLicenseNumberLabelClick");
			//Select Prior Bodily Injury Limits
			selectByVisibleText(ArbelaDriverInformationPage.DriverLicenseState,LicenseState, "DriverLicenseState");
			//Validate Driver License Number 
			getText(ArbelaDriverInformationPage.DriverLicenseNumbervalidation,"DriverLicenseNumbervalidation");
			//Select Driver Gender
			selectByVisibleText(ArbelaDriverInformationPage.DriverGender,Gender, "DriverGender");
			//Select Driver Martial Status
			selectByVisibleText(ArbelaDriverInformationPage.DriverMaritalStatus,MaritalStatus, "DriverMaritalStatus");
			//Select Drive rOccupation
			selectByVisibleText(ArbelaDriverInformationPage.DriverOccupation,Occupation, "DriverOccupation");
			//Select Driver Highest Education Level
			selectByVisibleText(ArbelaDriverInformationPage.DriverHighestEducationLevel,HighestEducationLevel, "DriverHighestEducationLevel");
			SuccessReportWithScreenshot("Connecticut Driver  Information Page", "Sucessfully Navigate to Connecticut Driver  Information Page");
			//Click Add Button
			click(ArbelaDriverInformationPage.DriverClickAddButton,"DriverClickAddButton");
			Thread.sleep(5000);
			//Fill ValidDLicenseNumber Information
			type(ArbelaDriverInformationPage.DriverLicenseNumber,ValidDLicenseNumber, "Enter Valid DLicenseNumber"); 
			SuccessReportWithScreenshot("Driver License Number message", "Sucessfully validating LicenseNumber message");			 
			//Click Add Button
			click(ArbelaDriverInformationPage.DriverClickAddButton,"DriverClickAddButton");
			Thread.sleep(5000);
    		//verify UnderWriterRuleMessage(driver,"Driver cannot be younger than 16 years.");
			//Verify Under Writer RuleMessage
			assertText(ArbelaDriverInformationPage.Driverliceancevalidationmessage,DriverValidationMessage);
			SuccessReportWithScreenshot("Driver liceance validation message", "Sucessfully validating Driverliceance message");
			//getText(ArbelaDriverInformationPage.DriverLicenseNumbervalidation,"DriverLicenseNumbervalidation");
			Thread.sleep(10000);
			//Fill Driver Dob Information
			type(ArbelaDriverInformationPage.DriverDob,DriverDob, "Enter valid Driver Dob");
			Thread.sleep(10000);
			SuccessReportWithScreenshot("Connecticut Driver  Information Page", "Sucessfully Navigate to Connecticut Driver  Information Page");
			//Click Add Button
			click(ArbelaDriverInformationPage.DriverClickAddButton,"DriverClickAddButton");
			Thread.sleep(10000);
		    click(ArbelaDriverInformationPage.DriverClickAddButton,"DriverClickAddButton");
			Thread.sleep(10000);
			SuccessReportWithScreenshot("Connecticut Driver  Information Page", "Sucessfully Navigate to Connecticut Driver  Information Page");
			//Click on Continue Button
			click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
		
			
			//Fill Vehicle VinNumber Information
			 type(ArbelaVehiclesCoveragesInformationPage.VehicleVinNumber,VehicleVinNumber, "Enter VehicleVinNumber");
			//Select Vehicle Type
			 selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.VehicleType,VehicleType, "Vehicle Type");
			//Select Vehicle Type
			 selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.VehicleType,VehicleType, "Vehicle Type");
			//Fill Vehicle Year Information
			// type(ArbelaVehiclesCoveragesInformationPage.VehicleYear,VehicleYear, "Enter VehicleYear");
			//Fill Vehicle Make Information
			// type(ArbelaVehiclesCoveragesInformationPage.VehicleMake,VehicleMake, "Enter VehicleMake");			 
			//Fill Vehicle Model Information
			// type(ArbelaVehiclesCoveragesInformationPage.VehicleModel,VehicleModel, "Enter VehicleModel");		 
			//Fill Collision Symbol Information
			// type(ArbelaVehiclesCoveragesInformationPage.VehicleCollisionSymbol,VehicleCollisionSymbol, "Enter VehicleCollisionSymbol");
			//Fill Comprehensive Symbol Information
			// type(ArbelaVehiclesCoveragesInformationPage.VehicleComprehensiveSymbol,VehicleComprehensiveSymbol, "Enter VehicleVinNumber");
			js.executeScript("window.scrollBy(0,200)");
			 //Fill Bodily Injury Information
			selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.VehicleBodilyInjury,BodilyInjury, "Enter Bodily Injury Number");
			//Fill Property Damage Information
			selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.VehiclePropertyDamage,PropertyDamage, "Enter Property Damage");
			//Select UM Split Type
			selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.UMSplit,UMSplit, "VehicleUM Split");
			//Select UM CSL Type
			//selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.UMCSL,UMCSL, "Vehicle UM CSL");
			//Select Principal Operator
			 selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.PrincipalOperator,PrincipalOperator, "Vehicle Principal Operator");
			//Select  Usage
			 selectByVisibleText(ArbelaVehiclesCoveragesInformationPage.Usage,Usage, "Vehicle  Usage"); 
			//Fill AnnualMileage Information
			 type(ArbelaVehiclesCoveragesInformationPage.AnnualMileage,AnnualMileage, "Enter AnnualMileage");
			 SuccessReportWithScreenshot("Connecticut Vehicles Coverages  Information Page", "Sucessfully Navigate to Connecticut Vehicles Coverages  Information Page");
			//Click Add Button
			 click(ArbelaDriverInformationPage.DriverClickAddButton,"DriverClickAddButton");
			 SuccessReportWithScreenshot("Connecticut Loss and Vilations Information Page", "Sucessfully Navigate to Loss and Vilations  Information Page");	 
			//Click on Continue Button
			 click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");	 
			 SuccessReportWithScreenshot("Connecticut Secured Lender and Lessor Information Page", "Sucessfully Navigate to Secured Lender and Lessor  Information Page");	 
			//Click on Continue Button
			 click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton"); 
			//Scroll Down the page
			 js.executeScript("window.scrollBy(0,1000)");
			 SuccessReportWithScreenshot("Connecticut QuoteSummary Information Page", "Sucessfully Navigate to QuoteSummary  Information Page");
			//Click on Continue Button
			 click(ArbelaAgencyInformationPage.ContinueQuoteSummary,"Click on ContinueButton");
			//Scroll Down the page
			 js.executeScript("window.scrollBy(0,1000)");
			//Select unansweredquestionstoNo" radio button
			 click(ArbelaUnderwritingquestionPage.unansweredquestionstoNo,"ClickunansweredquestionstoNo");
			 SuccessReportWithScreenshot("Connecticut Underwritingquestion Information Page", "Sucessfully Navigate to Underwriting question Information Page");
			//Click on Continue Button
			 click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
			 SuccessReportWithScreenshot("Connecticut Underwritingquestion Information Page", "Sucessfully Navigate to Underwriting question Information Page");
			//Click on Continue Button
			 click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
			//Select  Billing Method
			 selectByVisibleText(ArbelaPaymenPlanInformationPage.BillingMethod,BillingMethod, "Vehicle Billing Method");
			//Click on Paid in Full Discount
			 click(ArbelaPaymenPlanInformationPage.PaidinFullDiscount,"Click on PaidinFullDiscount Button");
			//Select  Payment Plan
			 selectByVisibleText(ArbelaPaymenPlanInformationPage.PaymentPlan,PaymentPlan, "Vehicle Payment Plan");
			//Select  Down Payment Type
			 selectByVisibleText(ArbelaPaymenPlanInformationPage.DownPaymentType,DownPaymentType, "Vehicle Down Payment Type");
			//Fill Down Payment Amount
			 type(ArbelaPaymenPlanInformationPage.DownPaymentAmount,DownPaymentAmount, "Enter Down Payment Amount");
			 SuccessReportWithScreenshot("Connecticut PaymenPlan Information Page  ", "Sucessfully Navigate to PaymenPlan Information Page");
			//Click on Continue Button
			 click(ArbelaAgencyInformationPage.ContinueButton,"Click on ContinueButton");
			//Click on PolicySummaryAgreecheckbox
			 click(ArbelaPolicySummaryObjects.PolicySummaryAgreecheckbox,"Click on PolicySummaryAgreecheckbox");
			//Click on AryarbellaSummaryButton
			 click(ArbelaPolicySummaryObjects.AryarbellaSummaryButton,"Click on AryarbellaSummaryButton");
			//Verify Under Writer RuleMessage
			 assertText(ArbelaPolicySummaryObjects.SubmissionConfirmation,"Submission Confirmation");
			 SuccessReportWithScreenshot("Submission Confirmation message", "Sucessfully Submit CT Form");	
			//Click on saveAndExitButton
			 click(ArbelaPolicySummaryObjects.saveAndExitButton,"Click on saveAndExitButton");
			
		} catch (Exception e) {
			e.printStackTrace();
			failureReport("Submit CT Agency application information page", "Failed to Click CT Agency application information page");
		}
	}
	
	
	public static void selectEffectiveDate(WebDriver driver) throws InterruptedException {

		Now n = new Now();

		Date d = new Date();

		String today = LocalTime.now().toString().replace(":", "_").replace(".", "_");

		SimpleDateFormat sd = new SimpleDateFormat("dd");

		today = sd.format(d);

		//driver.findElement(By.xpath("//*[@id='policyTerm']/div[1]/div/span/div/span")).click();
		//driver.findElement(By.xpath("//span[@class='input-group-addon']")).click();
		driver.findElement(By.xpath("//*[@id='policyTerm']/div[1]/div/span/div/span/i")).click();
        Thread.sleep(5000);
		// find the calendar
		List<WebElement> columns = driver.findElements(By.xpath("//div[@class='datepicker-days']//td[@class='today day']"));

		// comparing the text of cell with today's date and clicking it.
		for (WebElement cell : columns) {
			if (cell.getText().equals(today)) {
				cell.click();
				break;
			}
		}

	}
	

	public static void selectEffectiveDateApplicationinformationDOB(WebDriver driver) throws InterruptedException {

		Now n = new Now();

		Date d = new Date();

		String today = LocalTime.now().toString().replace(":", "_").replace(".", "_");

		SimpleDateFormat sd = new SimpleDateFormat("dd");

		today = sd.format(d);

		//driver.findElement(By.xpath("//*[@id='policyTerm']/div[1]/div/span/div/span")).click();
		driver.findElement(By.xpath("//*[@id='AppInfoSection']/div/div/span/div/span/i")).click();
        Thread.sleep(5000);
		// find the calendar
		List<WebElement> columns = driver.findElements(By.xpath("//div[@class='datepicker-days']//td[@class='today day']"));

		// comparing the text of cell with today's date and clicking it.
		for (WebElement cell : columns) {
			if (cell.getText().equals(today)) {
				cell.click();
				break;
			}
		}

	}

	
	
	public static void selectEffectiveDateDriverDetailsinformationDOB(WebDriver driver) throws InterruptedException {

		Now n = new Now();

		Date d = new Date();

		String today = LocalTime.now().toString().replace(":", "_").replace(".", "_");

		SimpleDateFormat sd = new SimpleDateFormat("dd");

		today = sd.format(d);

		//driver.findElement(By.xpath("//*[@id='policyTerm']/div[1]/div/span/div/span")).click();
		driver.findElement(By.xpath("//*[@id='DriverDetailSection']/div[4]/div/span/div/span/i")).click();
        Thread.sleep(5000);
		// find the calendar
		List<WebElement> columns = driver.findElements(By.xpath("//div[@class='datepicker-days']//td[@class='today day']"));

		// comparing the text of cell with today's date and clicking it.
		for (WebElement cell : columns) {
			if (cell.getText().equals(today)) {
				cell.click();
				break;
			}
		}

	}

	public static boolean verifyUnderWriterRuleMessage(WebDriver driver, String ExpectedAlertMessage)

	{

		boolean TrueFalse = false;

		WebElement alert_getAlertId = driver.findElement(By.id("alert-danger"));

		List<WebElement> alert_getAllTheAlerts = alert_getAlertId.findElements(By.xpath("//*[@id='alert-danger']/p"));

		System.out.println(alert_getAllTheAlerts.size());

		ExpectedAlertMessage = ExpectedAlertMessage.replace(" ", "");

		System.out.println(ExpectedAlertMessage);

		String ActualAlertMessage;

		for (int i = 0; i <= alert_getAllTheAlerts.size() - 1; i++)

		{

			ActualAlertMessage = alert_getAllTheAlerts.get(i).getText().replace(" ", "");

			// System.out.println(sActualAlertMessage);

			if (ActualAlertMessage.contains(ExpectedAlertMessage))

			{

				// System.out.println("Displayed");

				TrueFalse = true;

				break;

			}

		}

		return TrueFalse;

	}
	

	
	
	}





































